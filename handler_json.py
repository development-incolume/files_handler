from platform import python_version
if python_version() <= '3.0':
    print('Is necessary Python3')

from urllib.request import urlopen
from urllib.request import ProxyHandler
from urllib.request import build_opener
from urllib.request import install_opener
import json
import sys
import pprint

class Handler_json:
    DEBUG = 1
    def __init__(self, DEBUG=bool()):
        self.setDEBUG(DEBUG)

    def setDEBUG(self, DEBUG):
        flag = {False: False, '0':False, 0: False, True: True, '1':True, 1: True}
        if Handler_json.DEBUG: print('Debug: ', DEBUG)
        if DEBUG in flag:
            Handler_json.DEBUG = flag[DEBUG]
        else:
            Handler_json.DEBUG = False

    def json_read_web(self, url='https://pastebin.com/raw/wwe3HQtk', proxyon=True, proxies=dict()):
        list_proxies = {
            'http': 'http://10.1.101.101:8080',
            'ftp': 'http://10.1.101.101:8080',
            'https': 'http://10.1.101.101:8080',
        }
        try:
            if Handler_json.DEBUG: print(proxies)

            if proxyon and not proxies:
                proxies = list_proxies

            if Handler_json.DEBUG: print(proxies)
            if proxyon:
                # create the object, assign it to a variable
                proxy = ProxyHandler(proxies)
                # construct a new opener using your proxy settings
                opener = build_opener(proxy)
                # install the openen on the module-level
                install_opener(opener)
            data = urlopen(url, timeout=3).read().decode()
            data = json.loads(data)
            if Handler_json.DEBUG: print(type(data))
            if Handler_json.DEBUG: print(data)

            if Handler_json.DEBUG: print(len(data))
            if Handler_json.DEBUG: print(len(data['Brasil']))
            if Handler_json.DEBUG: print(data['Brasil'][0]['nome'])

            # print all
            for estado in data['Brasil']:
                print(estado)
            #
            for estado in data['Brasil']:
                print('\nNome: %s;\nCapital: %s;\nSigla: %s;\nRegião: %s;' %
                      (estado['nome'], estado['capital'],estado['sigla'],estado['regiao']))
            pass
        except:
            print(sys.exc_info())

    def json_download_web(self, url='https://pastebin.com/raw/wwe3HQtk', filename='', filepath='',
                          proxyon=True, proxies=dict()):
        try:
            list_proxies = {
            'http': 'http://10.1.101.101:8080',
            'ftp': 'http://10.1.101.101:8080',
            'https': 'http://10.1.101.101:8080',
            }
            if Handler_json.DEBUG: print(proxies)
            if Handler_json.DEBUG: print(type(proxies))
            if Handler_json.DEBUG: print(proxies.values())
            if Handler_json.DEBUG: print(type(proxies.values()))
            if proxyon and not proxies:
                proxies = list_proxies
            if proxyon:
                # create the object, assign it to a variable
                proxy = ProxyHandler(proxies)
                # construct a new opener using your proxy settings
                opener = build_opener(proxy)
                # install the openen on the module-level
                install_opener(opener)
            data = urlopen(url, timeout=3).read().decode()
            data = json.loads(data)
            if Handler_json.DEBUG: pprint.pprint(data)
            if not filename:
                filename = url.split('/')[-1]
                if Handler_json.DEBUG: print(filename.find('.json'))
                if filename.find('.json') < 0:
                    filename += '.json'

            if not filepath:
                filename = 'data/'+ filename
            else:
                filename = filepath + '/' + filename


            if Handler_json.DEBUG: print(filename)
            filehandler = open(filename, mode='w')
            json.dump(data, filehandler, sort_keys = True, indent = 4, ensure_ascii=False)
            filehandler.close()
            return True
            pass
        except Exception as e:
            print(sys.exc_info())

    def json_read(self, filename):
        try:
            handler = open(filename)
            handler = handler.read()
            if Handler_json.DEBUG: print(type(handler))
            if Handler_json.DEBUG: pprint.pprint(handler)
            return handler
            pass
        except:
            print(sys.exc_info())


    def json_write(self, filename, values=dict()):
        try:
            with open(filename, 'w') as handler:
                #json.dump(values, handler)
                json.dump(values, handler, sort_keys = True, indent = 4, ensure_ascii=False)
            return True
            pass
        except:
            print(sys.exc_info())

    def teste01(self):
        a = Handler_json().json_read_web(proxyon=False, proxies={'https':'10.1.101.101:8080'})
        b = Handler_json().json_read_web(proxyon=True, proxies={'http':'10.1.101.101:8080'})
        c = Handler_json().json_read_web(proxyon=False)
        d = Handler_json().json_read_web()
        pass
    def teste02(self):
        #a = Handler_json().json_download_web()
        #b = Handler_json().json_download_web(url='https://gitlab.com/development-incolume/incolumepy.data/raw/master/brasil.json')
        #c = Handler_json().json_download_web(proxies='10.100.0.14:8080')
        d = Handler_json().json_download_web(proxies={'https':'10.100.0.14:8080'},
                                             url='https://gitlab.com/development-incolume/incolumepy.data/raw/master/brasil.json')
        pass
    def teste03(self):
        Handler_json().json_read(filename='data/brasil.json')

    def teste04(self):
        Handler_json().json_write(filename='data/arq.json',values={'pessoa':[{'altura':0.0, 'peso':0.0}]})

    def teste_casa(self):
        Handler_json().json_download_web(
            url='https://gitlab.com/development-incolume/incolumepy.data/raw/master/brasil.json', proxyon=False)

        a = {"Notebooks": {
            "Notebook1": {"disco": "1TB", "ram": "128G", "tela": "14", "touchscreen": True },
            "Notebook2": {"disco": "1TB", "ram": "128G", "tela": "14", "touchscreen": True },
                           }
             }
        Handler_json().json_write(filename='data/computadores.json', values=a)
        Handler_json().json_read(filename='data/computadores.json')
        Handler_json().json_read_web(proxyon=False,
                                     url='https://gitlab.com/development-incolume/incolumepy.data/raw/master/brasil.json')
        Handler_json().json_download_web(url='https://pastebin.com/raw/JnGWD35N', proxyon=False)
        Handler_json().json_download_web(url='https://pastebin.com/raw/wwe3HQtk', proxyon=False)


    @staticmethod
    def main():
        #Handler_json().teste01()
        #Handler_json().teste02()
        #Handler_json().teste03()
        #Handler_json().teste04()
        Handler_json(1).teste_casa()

if __name__ == '__main__':
    pass
    Handler_json.main()
