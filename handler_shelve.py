import shelve
import sys
import io

from handler import Handler_file
from urllib.request import urlopen

class Handler_shelve(Handler_file):
    DEBUG=0
    def read_web(self, url):
        try:
            data = urlopen(url).read()
            stream = io.StringIO(data)
            stream.seek(0)

            print(stream, data)

            handler = shelve.open(stream)
            print(handler)

            pass
        except:
            print(sys.exc_info())

    def read(self, filename):
        try:
            handler = shelve.open(filename)
            for section, option in handler.items():
                print(section, option)

            for v in handler.values():
                print(v)

            dic = dict(handler)
            print(type(dic))
            print(dic)
            pass
        except:
            print(sys.exc_info())


    def write(self, filename, values=dict):
        try:
            with shelve.open(filename) as handler:
                handler.update(values)
            pass
        except:
            print(sys.exc_info())
    @staticmethod
    def main():
        dlist = {'server01': {'ip': '10.1.1.1', 'port': 8080},
                 'server02': {'ip': '10.1.1.2', 'port': 8080},
                 'server03': {'ip': '10.1.1.3', 'port': 8080},
                 'server04': {'ip': '10.1.2.255', 'port': 80, 'path': '/'},
                 }
        if Handler_shelve.DEBUG:
            Handler_shelve().write('data/brasil.dbm',dlist)
            Handler_shelve().read('data/brasil.dbm')
        Handler_shelve().read_web('https://gitlab.com/development-incolume/incolumepy.data/raw/master/brasil.dbm')


if __name__ == '__main__':
    pass
    Handler_shelve().main()