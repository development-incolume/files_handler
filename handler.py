import abc

class Handler_file(metaclass = abc.ABCMeta):

    @abc.abstractmethod
    def write(self):
        raise NotImplementedError('Metodo write não implementado')

    @abc.abstractmethod
    def read(self):
        raise NotImplementedError('Metodo read não implementado')
        pass

    @abc.abstractmethod
    def read_web(self):
        raise NotImplementedError('Metodo read_web não implementado')
        pass

    @abc.abstractclassmethod
    def main(cls):
        raise NotImplementedError('Metodo main não implementado')
        pass
