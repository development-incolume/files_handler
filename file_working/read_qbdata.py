'''
https://panda.ime.usp.br/pensepy/static/pensepy/10-Arquivos/files.html#metodos-alternativos-para-ler-arquivos
'''
arq = open("qbdata.txt")
linha = arq.readline()
print(linha)
arq.close()


arq = open("qbdata.txt", 'r')
linhas = arq.readlines()
print('total de linhas: {}'.format(len(linhas)))
print('total de linhas: %s' % (len(linhas)))

print(linhas[0:4])
arq.close()


with open("qbdata.txt", 'r') as arq:
    file = arq.read()
    print(len(file))
    print(file[:256])


print('*'*20)
def alpha():
    ref_arquivo = open("qbdata.txt","r")
    linha = ref_arquivo.readline()
    while linha:
        valores = linha.split()
        #print('>>>', valores)
        print('QB ', valores[0], valores[1], 'obteve a avaliacao ', valores[10] )
        linha = ref_arquivo.readline()
        linhas.append(linha)
    #return linhas
    ref_arquivo.close()

#print(alpha())
alpha()
