# See http://peak.telecommunity.com/DevCenter/setuptools#namespace-packages
import pkgutil, sys

try:
    __import__('pkg_resources').declare_namespace(__name__)
except ImportError:
    from pkgutil import extend_path
    __path__ = extend_path(__path__, __name__)




if __name__ == '__main__':
    try:
        print(__name__)
        #print(__path__)
        print(__import__('pkg_resources').declare_namespace(__name__))
        print(__import__('pkg_resources'))
        #print(pkgutil.extend_path(__path__, __name__))
    except:
        print(sys.exc_info())
